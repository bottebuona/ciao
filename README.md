Ciao Jekyll template documentation
==================================
Version: 1.0.0

Date of release: 11 oct 2017

## Software requirements:

* ruby
* jekyll
* bundler

## Installation

1. Install the theme gem on your system.
    ```bash
    gem install jekyll-theme-ciao-1.0.0.gem
    ```

2. Add the theme to the **Gemfile** of you Jekyll project:
    ```yaml
    gem "jekyll-theme-ciao"
    ```

3. <a id="step-add-files"></a>Add the content of the `Additional_files` folder **inside the root directory of your Jekyll project** ([What files are these?](#additional-files)).

4. Run `bundle install` to install the dependencies.

5. To locally test your website run `bundle exec jekyll serve`

6. For deployments methods refer to the [Jekyll documentation](https://jekyllrb.com/docs/deployment-methods/).

## Configuration

Configuration options are available inside `_config.yml`.

### Site title and logo

Logo image is shown inside the top navigation bar.

If `display_title` is set to `false`, the title will not be shown inside the top navigation bar.

```yaml
title: Blog Title
logo: images/logo.png
display_title: true
```

### Site tagline

If a a tagline is provided it will be shown inside the website header.

```yaml
tagline: This is the blog tagline</br>Keep it short!
```
### Site description

Site description will appear in your document head meta (for Google search results) and in your feed.xml site description.

```yaml
description: An awesome description for your new site here.
```

### Social links

The website social links. They are displayed in the site footer.

```yaml
# Site social links
social:
    github:
    instagram:
    twitter:
    facebook:
    tumblr:
    pinterest:
    linkedin:
    google-plus:
```

### Site author

The default author for the website. You can override it inside the Front Matter block of posts.

```yaml
# Site author - default author data displayed in posts
author:
    name: Jane Div
    bio: Jane is a professional fictitious person. She writes dummy content for demo websites like this one.
    avatar: images/authors/jane-profile-picture.jpg
    social:
      facebook:
      github: http://github.com
      instagram:
      twitter:
      google-plus:
      tumblr:
      pinterest:
      linkedin:
```

### Pagination

Customize home page [pagination](https://jekyllrb.com/docs/pagination/):

```yaml
paginate: 6
paginate_path: /page:num/
page_links: 5
```

* `paginate` change the number of post per page.
* `paginate_path` change the destination of pagination pages.
* `page_links`, change the number of links to other pagination pages.

### Theme colors

Change the theme colors.

```yaml
# Colors
navbar_background: "#984D81"
tagline_background: "#E6568D"
link_color: "#9095DF"
```

* `navbar_background` is the *navigation bar* background color.
* `tagline_background` is the *tagline container* background color.
* `link color` is the color of *text links* in posts and pages.

### Comments

Enable [Disqus](https://disqus.com/) comments. Jekyll will show the comment section below each post.

```yaml
disqus_shortname: your-site-disqus-shortname
```
If you need a guide for how to obtain a disqus shortname, refer to [this section of Disqus documentation](https://help.disqus.com/customer/en/portal/articles/466208-what-s-a-shortname-).

### Site search

Enable search using Google Custom Search Engine (CSE).

```yaml
google_cse_id: your-google-custom-search-engine-id
```

To obtain a Google CSE id,  create a search engine for your website at [Google CSE page](https://cse.google.com).

If you leave the `google_cse_id` field empty, the search box inside top navigation bar will not be displayed.


## Writing content

### Posts

Create new posts inside the `_posts` directory using the [Jekyll naming conventions](https://jekyllrb.com/docs/posts/#creating-post-files).

### Pages

Theme pages are stored in `pages` directory. You can add your pages there or choose another folder. For further informations about additional pages refer to the [Jekyll documentation](https://jekyllrb.com/docs/pages/#where-additional-pages-live).

### Available layout

The theme makes available a `post` layout and a `page` layout, used for post and pages.

### Tags and categories

The theme will automatically generate a *tags page* and a *categories page*. This two pages consist of a list of all posts grouped by tags or categories. Clicking on a category or on a tag inside a post, will open the corresponding page to the appropriate section.

If none of your posts has a category, the link to the categories page inside the top navigation bar will not be displayed.

### Drop caps

<span class="dropcap">C</span>iao includes the [Adobe drop caps javascript plugin](http://webplatform.adobe.com/dropcap.js/). To use it in your posts, wrap the target letter inside a `span` tag with the class `dropcap`.

```html
<span class="dropcap">J</span>ekyll.
```

## Posts custom options

You can set some options inside the **Front Matter block of posts**.

### Post image

Choose an image to be used when the post is listed inside the home page or in the related posts section.

```yaml
image: images/posts/post_image.jpg
```

For the image to be displayed without cropping it must have a width/height ratio of 16:9.

### Custom author

Override the site default author.

```yaml
    author:
        name: John
        avatar: images/authors/john_avatar.jpg
        social:
            facebook: http://www.facebook.com
            twitter: http://www.twitter.com
 ```

### Show/Hide sections

You can choose to show or hide author, social sharing links and comments.

```yaml
display_author: false
display_share_links: false
comments: false
```

## Additional files

As one of the installation steps [you are asked to add some additional files](#step-add-files). These file are needed by the theme to properly work:

* An index file with the `.html` extension, needed for pagination to work.
* A 404 page consistent with the look of the theme.
* A set of custom pages contained inside a `pages` folder: *tags* page, *categories* page, and *search results* page.
* A customized `_config.yml` file.

Alternatively, if you already have a customized `_config.yml` file, you can only add the missing parts:

```yaml
# _config.yml file for jekyll-theme-ciao

# Site settings
title: Your awesome title
tagline: This is the blog tagline<br>lorem ipsum
logo:
display_title: true
email: your-email@example.com
description: An awesome description for your new site here.
baseurl: "" # the subpath of your site, e.g. /blog
url: "" # the base hostname & protocol for your site, e.g. http://example.com

# Pagination settings
paginate: 6
paginate_path: /page:num/
page_links: 5

# Site author - default author data displayed inside posts
author:
  name: Jane Div
  bio: Biographic information about the author.
  avatar: images/authors/author-profile-picture.jpg
  social:
    github:
    instagram:
    twitter:
    facebook:
    tumblr:
    pinterest:
    linkedin:
    google-plus:

# Site social - links will be displayed in every page footer
social:
  rss:
  github:
  instagram:
  twitter:
  facebook:
  tumblr:
  pinterest:
  linkedin:
  google-plus:

# Comments
disqus_shortname:

# Search
google_cse_id:  

# Colors
link_color: "#9095DF"
navbar_background: "#984D81"
tagline_background: "#E6568D"

# Build settings
markdown: kramdown
theme: jekyll-theme-ciao
plugins:
  - jekyll-feed
  - jekyll-paginate
  - jekyll-seo-tag

# Default settings
defaults:
  # _posts
  -
    scope:
      path: ""
      type: posts
    values:
      layout: post
      comments: true
      display_share_links: true
      display_author: true
  -
    scope:
      path: ""
      type: pages
    values:
      comments: false
```

## Credits

Jekyll plugins used for this theme:

* jekyll-feed
* jekyll-paginate
* jekyll-seo-tag

Libraries and other resources:

* [Bootstrap](http://getbootstrap.com).
* [jQuery](https://jquery.com/).
* [Font Awesome](http://fontawesome.io) icons.

Images for the demo website:

* Post images from [Pixabay](http://pixabay.com).
* Author's profile pictures from [randomuser.me](https://randomuser.me/) through [bootbundle](http://www.bootbundle.com).
* Website logo is one of the [Entypo pictograms](http://www.entypo.com/) by Daniel Bruce.
