# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-ciao"
  spec.version       = "1.0.0"
  spec.authors       = ["Mic Themes"]
  spec.email         = ["micthemes@gmail.com"]

  spec.summary       = %q{Simple and clean blog template for Jekyll}
  spec.homepage      = "https://ciao.netlify.com"
  spec.license       = "Nonstandard"

  spec.files = `git ls-files -z`.split("\x0").select do |f|
      f.match(%r{^(assets|_(includes|layouts|sass)/|(LICENSE|README)((\.(txt|md|markdown)|$)))}i)
  end
  spec.add_runtime_dependency "jekyll", "~> 3.5"
  spec.add_runtime_dependency "jekyll-paginate", "~> 1.1"
  spec.add_runtime_dependency "jekyll-seo-tag", "~> 2.1"

  spec.add_development_dependency "bundler", "~> 1.12"
end
