---
layout: page
title: About
permalink: /about/
type: page
---

This is the Ciao theme for Jekyll. You can find out more info about using Ciao Jekyll theme at [Ciao docs](http://ciao-doc.netlify.com). Basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

You can find the source code for Jekyll at GitHub:
[jekyll][jekyll-organization] /
[jekyll](https://github.com/jekyll/jekyll)

[jekyll-organization]: https://github.com/jekyll
