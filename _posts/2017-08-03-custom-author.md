---
title: Custom author
category: usage
tags: [front matter, author]
author:
    name: John Span
    bio: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    avatar:  images/authors/men13.jpg
    social:
        facebook: http://www.facebook.com
        twitter: http://www.twitter.com
image: images/posts/custom-author.jpg
excerpt: The Front Matter block of this post has an author section. It overrides the default site author.
---

![biker]({{ 'images/posts/custom-author.jpg' | absolute_url }})

The [Front Matter](https://jekyllrb.com/docs/frontmatter/) block of this post has an author section.

```yaml
author:
    name: John Span
    bio: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    avatar: images/authors/john_avatar.jpg
    social:
        facebook: http://www.facebook.com
        twitter: http://www.twitter.com
```

This overrides the default site author.
