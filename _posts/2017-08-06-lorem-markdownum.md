---
title: Lorem Markdownum
categories: markdown demo
tags:
    dummy content
    loremipsum
image: images/posts/panorama.jpg
---

<span class="dropcap">L</span>orem markdownum tamen, hos deae **metus** abstrahere facinus et rigidi, dixisse
totumque **orbam**, ducis. Est *hinc Ceres* pro [qui virgineas
fidae]({{ page.url | absolute_url }}), sed viro.

Ecquid et fraga tam veniebat corpora, quoque spectare demersus. Numen Somnus!
Urbis multi castris per meae Iovis corpore enituntur numina; nullisque non egere
rependis, vidi arbore.

## Fama secum

Ludica pariter compescuit honores Amathunta acceptissimus altos simul ut *caeli
navis* munera nympha cornum. Mala ore quod ob stramine, attritas, rumorum
rostrum sed dederat litus bello.

1. Bracchia flumina ipsam flagellis
2. Cum fuit iurasse aper vagatur credensque tela
3. Tuis servat suo rogat rota quaque
4. Et virum per Horis positis coniungere
5. Ardeat vivo canori quae danda
6. Cursus nubes

## Nonne Pandion omnes patrique

<figure>
	<img src="{{ 'images/posts/panorama.jpg' | absolute_url }}">
	<figcaption>Lorem ipsum figure caption</figcaption>
</figure>

Deque dant Idas partes saepe plausit, pone sua? Hunc neque totis, terraque,
colores, volucris ab dextra ramos, cupido.

> Os verba **inferius precor** et adhuc vident crudelem saliunt et sentit, atra.
> Per sit hoc contortum agri Hercule habet Lernaeae obscenique possit, est
> laborum aevo. Hanc mihi resque integer dei prima si moriri inseritur Thisbes,
> esset primo per. Vale induit anili?

Sub omnis nondum belua prendique sertis passis quem, nec sic tetigere vestibus o
aspicit ingratus nasci. Parentis nimiique metu.
