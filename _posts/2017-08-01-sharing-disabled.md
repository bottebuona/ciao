---
title: Sharing links disabled
category: usage
tags: yaml social override
display_share_links: false
image: images/posts/secret.jpg
---
This post has no sharing links at the end of the article. To obtain this, add the following expression.

```yaml
display_share_links: false
```
inside the post [Front Matter](https://jekyllrb.com/docs/frontmatter/) section.
