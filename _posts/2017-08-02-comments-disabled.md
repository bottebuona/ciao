---
title: Comments disabled
comments: false
image: images/posts/industrial.jpg
excerpt: "This post shows no comments area. This is because it has the line `comment: false` in the post Front Matter section."
category: usage
tags: [comments, front matter]
---
![baloon]({{ 'images/posts/industrial.jpg' | absolute_url }})

This post has no comments area. This is because in the post [Front Matter](https://jekyllrb.com/docs/frontmatter/) there is this line:

```yaml
comments: false
```
